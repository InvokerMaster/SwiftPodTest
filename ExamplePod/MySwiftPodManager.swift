//
//  MySwiftPodManager.swift
//  ExamplePod
//
//  Created by Invoker on 4/7/19.
//  Copyright © 2019 Lazlo326. All rights reserved.
//

import Foundation

public class MySwiftPodManager: NSObject {
    
    public func getMyTest(value: String) -> String {
        return "Hello \(value)"
    }
    
}
